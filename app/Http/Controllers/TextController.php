<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Closure;

class TextController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show()
    {
        return View::first(['text.show']);
    }

    public function process(Request $request)
    {
        $content = strtolower($request->post('content'));
        preg_match_all('/([a-z|A-Z|-]{3,})/', $content, $split);
        $split = array_count_values($split[1]);
        arsort($split);

        return \response()->json($split);
    }
}
