<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PHP - trivial task // Łukasz Krawczyk</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/grid/">

    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
    <link rel='stylesheet' href="/css/bootstrap-grid.min.css">
    <link rel='stylesheet' href="/css/grid.css">

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>

<body>
<div class="container" id="vueApp">

    <h1>Welcome to the Application</h1>
    <p class="lead">Paste into textarea whatever text in order to get words count!</p>


    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col">
                    <div class="container">
                        <div>
                            <textarea v-model="content" style="height:100px;width:300px;"></textarea></div>
                        <div>
                            <button v-on:click="sendReq">Count words!</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col">
                    <div class="container">
                        <strong>You submitted:</strong> <br/>
                        <div class="gray">
                            @{{ submitted }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col">
                    <div class="container">

                        <table style="height: 100px; background-color: rgba(255,0,0,0.1);" v-if="postResults">
                            <thead>
                            <tr>
                                <th style="width: 50px">#</th>
                                <th style="width: 300px;">Word</th>
                                <th style="width: 300px;">Count</th>
                                <th>Stars</th>
                                <!-- and so on -->
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(item, idx, x) in postResults"
                                :class="{'bg-secondary': idx % 2 === 0, 'bg-info': idx % 2 !== 0 }">
                                <td>@{{ x + 1 }}</td>
                                <td style="width: 300px;">@{{ idx }}</td>
                                <td style="width: 300px;">@{{ item }}</td>
                                <td v-if="x < 3"><span v-for="i in (3-x)">*</span></td>
                                <td v-else="x > 3">-</td>
                                <!-- and so on -->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /container -->


<script>
    new Vue({
        el: '#vueApp',
        data: {
            debug: true,
            content: 'default text',
            submitted: 'Nothing yet ;)',
            ajaxRequest: false,
            postResults: []
        },
        methods: {
            sendReq: function () {
                this.ajaxRequest = true;
                axios.post('/text/process', {
                    content: this.content,
                }).then(({data}) => {
                    this.postResults = data,
                        this.submitted = this.content
                })
            }
        }
    });
</script>¬

</body>
</html>
